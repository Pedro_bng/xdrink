const functions = require("firebase-functions");

var admin = require("firebase-admin");

admin.initializeApp(functions.config().firebase);


//var serviceAccount = require("../xdrink-d55ba-firebase-adminsdk-7izca-c76c6b73c8.json");

/*admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://xdrink-d55ba.firebaseio.com"
});*/



// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
 exports.helloWorld = functions.https.onRequest((request, response) => {
   functions.logger.info("Hello logs!", {structuredData: true});
   response.send("Hello from Firebase!");
 });

 exports.newCommande = functions.firestore
    .document('infoCommande/{userId}')
    .onCreate((snap, context) => {
      // Get an object representing the document
      // e.g. {'name': 'Marie', 'age': 66}
      const newValue = snap.data();

      // access a particular field as you would any JS property
      const name = newValue.name;

      const vendeurToken = newValue.vendeurToken;

<<<<<<< HEAD
<<<<<<< HEAD
=======
      var cmdCode = newValue.cmdCode;

>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
=======
      var cmdCode = newValue.cmdCode;

>>>>>>> 3b82dec (qr code)
      var payload = {
        notification: {
          title: "Nouvelle commande",
          body: "Cliquez pour accepter"
        }
      };

      var options = {
        priority: "high",
        timeToLive: 60 * 60 *24
      };

      return admin.messaging().sendToDevice(vendeurToken, payload, options);

<<<<<<< HEAD
<<<<<<< HEAD
=======
=======
>>>>>>> 3b82dec (qr code)
      //si le vendeur choisi n'accepte pas la commande au bout de 5 minutes
      //on le supprime de la commande 
      //et on réatribut la commande à un autre vendeur
      setTimeout(eliminationVendeur(cmdCode), 10000);
     

<<<<<<< HEAD
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
=======
>>>>>>> 3b82dec (qr code)
    });

 exports.livreurProcess = functions.firestore
    .document('infoCommande/{userId}')
    .onUpdate((change, context) => {
      // Get an object representing the document
      // e.g. {'name': 'Marie', 'age': 66}
      const newValue = change.after.data();

      // ...or the previous value before this update
      const previousValue = change.before.data();

      // access a particular field as you would any JS property
     // const name = newValue.name;
<<<<<<< HEAD

      const previousStatut = previousValue.statut;
      const newStatut = newValue.statut;
      const vendeur_choisi = newValue.vendeur_choisi;
      const cmdCode = newValue.cmdCode;


      //recuperation des coordonnées du vendeur choisi (marche uniquement avec les depot marchands pour l'instant)
      var lat2;
      var lon2;
      admin.firestore().collection('vendeur').where('nom_depot', '==', vendeur_choisi).onSnapshot(
        (querySnapchot) => {
          querySnapchot.forEach((doc) => {
            lat2 = doc.data().latitude;
            lon2 = doc.data().longitude;

          })
        }
      )
      

      //recuperation du livreur le plus proche apres acceptation du vendeur
      if(previousStatut === 'enregistrer' && newStatut === 'en_attente') {
          admin.firestore().collection('livreur').where('statut', '==', 'inactif').onSnapshot(
               (querySnapchot) => {
                  var unit = "K";
                  var distanceMin;
                  var distance;
                  var distances = [];
                  var detDistance = [];
                  var livreur_choisi_email = [];
                  var distances2 = [];
                  querySnapchot.forEach((doc1) => {
                    var lat1 = doc1.data().latitude;
                    var lon1 = doc1.data().longitude;

                    distance = getDistance(lat1, lon1, lat2, lon2, unit) ;

                    distances.push(distance);

                    detDistance.push({
                      emailLivreur: doc1.data().email,
                      dist: distance
                    })

                  })
                  //recuperation de la distance minamale
                  distanceMin = Math.min(... distances);

                  // comparaison de la distanceMin au distances des livreurs pour recuperer le plus proche 
                  detDistance.forEach(detDistx => {
                    if(detDistx.dist === distanceMin) {
                      livreur_choisi_email = detDistx.emailLivreur;
                      //changement du statut du livreur (inactif => actif)
                      admin.firestore().collection('livreur').where('email', '==', livreur_choisi_email).onSnapshot(
                        (querySnapchot) => {
                          querySnapchot.forEach((doc) => {
                            admin.firestore().collection('livreur').doc(doc.id).update({
                              statut: 'actif',
                              pour_commande: cmdCode
                            });
                          })
                        }
                      )

                      //envoie de notification au livreur
                      var payload = {
                        notification: {
                          title: "Nouvelle livraison",
                          body: "Cliquez pour plus de details"
                        }
                      };

                      var options = {
                        priority: "high",
                        timeToLive: 60 * 60 *24
                      };

                      admin.firestore().collection('testtoken').where('userid', '==', livreur_choisi_email).onSnapshot(
                        (querySnapchot) => {
                          querySnapchot.forEach((doc) => {
                            var tokLivreur = doc.data().usertoken;

                            return admin.messaging().sendToDevice(tokLivreur, payload, options);
                          })
                        }
                      )

                    }
                  })

                  
            

              }
          )
      }

    });




    function livreurChoisi (livreurPreselectionner = [], lat2, lon2) {
      livreurPreselectionner.forEach(livreurPreselectx => {
        admin.firestore().collection('livreur').where('email', '==', livreurPreselectx).onSnapshot(
          (querySnapchot) => {
            var meilleur_distance;
            var distances = [];
            var distance
            var detDistance = [];
            querySnapchot.forEach((doc) => {
              var lat1 = doc.data().latitude;
              var lon1 = doc.data().longitude;
              
              distance = getDistance(lat1, lon1, lat2, lon2, "K")

              distances.push(distance);

              detDistance.push({
                emailLiv: doc.data().email,
                distance: distance
              })

            })

           meilleur_distance  = Math.min(...distances);

           
           detDistance.forEach(detDistx => {
             if (detDistx.distance === meilleur_distance) {
               //changement du statut du livreur selectionner (en_etude => actif)
               admin.firestore().collection('livreur').where('email', '==', detDistx.emailLiv).onSnapshot(
                 (querySnapchot) => {
                   querySnapchot.forEach((doc) => {
                    admin.firestore().collection('livreur').doc(doc.id).update({
                      statut: 'actif'
                    })

                   })
                   return detDistx.emailLiv

                 }
               )
             }


           })

          }
        )
      })
    }
=======
     
      const livreurToken = newValue.livreurToken;
      const en_attente = newValue.en_attente;
      const en_livraison = newValue.en_livraison;


      //recuperation du livreur le plus proche apres acceptation du vendeur
      if(en_attente === 'ok' && en_livraison === '') { 
        var payload = {
          notification: {
            title: "Nouvelle livraison",
            body: "Cliquez pour accepter"
          }
        };
  
        var options = {
          priority: "high",
          timeToLive: 60 * 60 *24
        };
  
        return admin.messaging().sendToDevice(livreurToken, payload, options);
      }
         
    });

    function eliminationVendeur(cmdCode) { 
      admin.firestore().collection('infoCommande').where('cmdCode','==', cmdCode).get().then(
        (querySnapchot) => {
          var en_attente;
          querySnapchot.forEach((doc) => {
            en_attente = doc.data().en_attente
            if(en_attente === '') {
              
            }
          })
          
          
<<<<<<< HEAD
=======

        }
      )
     }

>>>>>>> 3b82dec (qr code)

        }
      )
     }
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd




<<<<<<< HEAD
    function getDistance (lat1, lon1, lat2, lon2, unit) {
      if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
      }
      else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        var radtheta = Math.PI * theta/180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
          dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return dist;
      }
    }    
=======
    
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
    

