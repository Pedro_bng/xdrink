import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Geolocation} from '@capacitor/core';
import { AuthentificationService } from '../authentification/authentification.service';

import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from '@capacitor/core';

const { PushNotifications, Modals } = Plugins;

@Component({
  selector: 'app-inscription-hotel-client',
  templateUrl: './inscription-hotel-client.page.html',
  styleUrls: ['./inscription-hotel-client.page.scss'],
})
export class InscriptionHotelClientPage implements OnInit {

  dataHotel = {
    nom_hotel: '',
    nom: '',
    telephone_gerant: '',
    email: '',
    ifu: '',
    adresse_livraison: '',
    password: '',
    password2: '',
    type: 'client',
    latitude: 0,
    longitude: 0,
    precision: 'hotel',
  };
  errorMessage: any;

  constructor(private authService: AuthentificationService,
              private router: Router,
              public alertController: AlertController,
              private firestore: AngularFirestore) { }

  ngOnInit() {
  }

  soumettre() {
    if (this.dataHotel.password === this.dataHotel.password2) {
      if (this.dataHotel.latitude !== 0 && this.dataHotel.longitude !== 0) {
      const email = this.dataHotel.email;
      const password = this.dataHotel.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataHotel.nom;
          localStorage.email = this.dataHotel.email;
          localStorage.type = 'client';
          this.generateTokenFCM();
          this.firestore.collection('client').add(this.dataHotel);
          this.firestore.collection('user').add(this.dataHotel);
          this.firestore.collection('portefeuille').add({
            proprietaire: this.dataHotel.email,
            solde: 0,
            retrait: 0,
            ajout: 0
          });
          localStorage.latitude = this.dataHotel.latitude;
          localStorage.longitude = this.dataHotel.longitude;
          localStorage.nom_hotel = this.dataHotel.nom_hotel;
          this.router.navigate(['tabs']);
        },
        (error) => {
          this.errorMessage = error;
        }
      ); 
     }else {this.presentAlertConfirm2();}
    }
  }

  async presentAlertConfirm2() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Activez votre localisation et ressayez</strong>!!!',
      buttons: [
         {
          text: 'ok',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Avant de confirmer la géolocalisation vous devez être dans votre Hotel</strong>!!!',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmer position',
          handler: () => {
            console.log('Confirm Okay');
            this.getPosition();
          }
        }
      ]
    });

    await alert.present();
  }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    this.dataHotel.latitude = position.coords.latitude;
    this.dataHotel.longitude = position.coords.longitude;
    console.log(this.dataHotel.latitude, this.dataHotel.longitude);
  }


  generateTokenFCM() {
    // Request permission to use push notifications
      // iOS will prompt user and return if they granted permission or not
      // Android will just grant without prompting
      PushNotifications.requestPermission().then(result => {
        if (result.granted) {
          // Register with Apple / Google to receive push via APNS/FCM
          PushNotifications.register();
        } else {
          // Show some error
        }
      });
  
      PushNotifications.addListener(
        'registration',
        (token: PushNotificationToken) => {
          alert('Push registration success, token: ' + token.value);
          this.firestore.collection('testtoken').add({
            userid: this.dataHotel.email,
            usertoken:token.value
          })
        },
      );
  
      PushNotifications.addListener('registrationError', (error: any) => {
        alert('Error on registration: ' + JSON.stringify(error));
      });
  
      PushNotifications.addListener(
        'pushNotificationReceived',
        (notification: PushNotification) => {
          // alert('Push received: ' + JSON.stringify(notification));
  
          // tslint:disable-next-line:prefer-const
          let alert =  Modals.alert ( {
            title: notification.title,
            message: notification.body
          });
        },
      );
  
      PushNotifications.addListener(
        'pushNotificationActionPerformed',
        (notification: PushNotificationActionPerformed) => {
          alert('Push action performed: ' + JSON.stringify(notification));
        },
      );
    }



}
  
  
  
