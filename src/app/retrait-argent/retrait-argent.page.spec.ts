import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RetraitArgentPage } from './retrait-argent.page';

describe('RetraitArgentPage', () => {
  let component: RetraitArgentPage;
  let fixture: ComponentFixture<RetraitArgentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetraitArgentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RetraitArgentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
