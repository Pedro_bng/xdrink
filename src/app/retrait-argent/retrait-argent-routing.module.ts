import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RetraitArgentPage } from './retrait-argent.page';

const routes: Routes = [
  {
    path: '',
    component: RetraitArgentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RetraitArgentPageRoutingModule {}
