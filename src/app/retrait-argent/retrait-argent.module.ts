import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RetraitArgentPageRoutingModule } from './retrait-argent-routing.module';

import { RetraitArgentPage } from './retrait-argent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RetraitArgentPageRoutingModule
  ],
  declarations: [RetraitArgentPage]
})
export class RetraitArgentPageModule {}
