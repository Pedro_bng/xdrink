import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthentificationService } from './authentification/authentification.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit  {
  client: boolean;
  vendeur: boolean;
  livreur: boolean;
  side: string;
  userType = '';
  statut: string;
  pseudo: string;
  email: string;
  userId: string;
  email2: string;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private authService: AuthentificationService,
    public afAuth: AngularFireAuth,
    public firestore: AngularFirestore
  ) {
    this.initializeApp();
  }


  ngOnInit() {
  }

  onMenuOpen() {
    this.pseudo = localStorage.getItem('pseudo');
    this.email = localStorage.getItem('email');
    this.userType = localStorage.getItem('user-type');
    this.statut = localStorage.getItem('type');
    console.log(this.statut);
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.demarrage();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //this.demmarage2();
    });
  }

  deconnexion() {
    this.authService.signOut();
    this.router.navigate(['connexion-client']);
    localStorage.removeItem('pseudo');
    localStorage.removeItem('email');
    localStorage.removeItem('user-type');
    localStorage.removeItem('type');
    localStorage.removeItem('nom_dep');
    localStorage.removeItem('latitude');
    localStorage.removeItem('longitude');
    localStorage.removeItem('cart');
    localStorage.removeItem('stock_vendeur');
    localStorage.removeItem('nom_distributeur');
    localStorage.removeItem('nom_hotel');
    localStorage.removeItem('nom_bar');
  }

  demarrage() {
    this.afAuth.authState.subscribe(auth => {
      if (!auth) {
        console.log('non connecter');
        //verifions si il est un livreur affilier en attente de validation de compte par sa société
        var stat = localStorage.getItem('type');
        var email = localStorage.getItem('email');
        if(stat === 'livreur') {
          this.firestore.firestore.collection('livreur_affilier').where('email', '==', email ).get().then(
            (querySnapchot) => {
              if(querySnapchot.empty) {

              } 
              if(!querySnapchot.empty) {
                this.router.navigate(['livreur-affilier-code']);
              }
             
            }
          )
        }
      }else {
        console.log('connecter');
        this.userId = auth.uid;
        this.email2 = auth.email;
        console.log(this.userId, this.email2);
        this.firestore.firestore.collection('user').where('email', '==', this.email2).onSnapshot(
          (querySnapshot) => {
            querySnapshot.forEach((doc) => {
              console.log(doc.data().type);
              if (doc.data().type === 'vendeur') {
                this.router.navigate(['vendeur-tabs']);
              } else if (doc.data().type === 'client') {
                this.router.navigate(['tabs']);
              }else if (doc.data().type === 'livreur') {
                this.router.navigate(['livreur-tabs']);
              }
            });
          }
        );
      }
    });
  }

  demmarage2(){
    var stat = localStorage.getItem('type')
    console.log(stat);
    if (stat === 'vendeur') {
      this.router.navigate(['vendeur-tabs']);
    } else if (stat === 'client') {
      this.router.navigate(['tabs']);
    } else if (stat === 'livreur') {
      this.router.navigate(['livreur-tabs']);
    }
  }

}

