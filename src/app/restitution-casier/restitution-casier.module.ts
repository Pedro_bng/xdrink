import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestitutionCasierPageRoutingModule } from './restitution-casier-routing.module';

import { RestitutionCasierPage } from './restitution-casier.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestitutionCasierPageRoutingModule
  ],
  declarations: [RestitutionCasierPage]
})
export class RestitutionCasierPageModule {}
