import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestitutionCasierPage } from './restitution-casier.page';

const routes: Routes = [
  {
    path: '',
    component: RestitutionCasierPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestitutionCasierPageRoutingModule {}
