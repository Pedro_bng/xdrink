import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VenteLivreurPage } from './vente-livreur.page';

const routes: Routes = [
  {
    path: '',
    component: VenteLivreurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VenteLivreurPageRoutingModule {}
