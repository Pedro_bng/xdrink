import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-historique-livreur',
  templateUrl: './historique-livreur.page.html',
  styleUrls: ['./historique-livreur.page.scss'],
})
export class HistoriqueLivreurPage implements OnInit {
  email: string;
  historique_livraison: any[];

  constructor(private firestore: AngularFirestore) {
    this.email = localStorage.getItem('email');
   }

  ngOnInit() {
    this.historique();
  }


  historique() {
    this.firestore.firestore.collection('infoCommande').where('livreur_choisi', '==', this.email ).onSnapshot(
      (querySnapchot) => {
<<<<<<< HEAD
        this.historique_livraison = [];
        querySnapchot.forEach((doc) => {
          this.historique_livraison.push({
            cmdCode: doc.data().cmdCode,
            statut: doc.data().statut
=======
        let enregistrer;
        let en_attente;
        let en_livraison;
        let remise;
        let livrer;
        let statut;
        this.historique_livraison = [];
        querySnapchot.forEach((doc) => {
          enregistrer = doc.data().enregistrer,
          en_attente = doc.data().en_attente,
          en_livraison = doc.data().en_livraison,
          remise = doc.data().remise,
          livrer = doc.data().livrer
          if(enregistrer === 'ok' && en_attente === '' && en_livraison === '' && remise === '' && livrer === ''){
            statut = 'enregistrer'
          }
          if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === '' && remise === '' && livrer === ''){
            statut = 'en_attente'
          }
          if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === '' && livrer === ''){
            statut = 'en_livraison'
          }
          if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === 'ok' && livrer === ''){
            statut = 'remise'
          }
          if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === 'ok' && livrer === 'ok'){
            statut = 'livrer'
          }
          this.historique_livraison.push({
            cmdCode: doc.data().cmdCode,
            statut: statut,
            heure: doc.data().heure_enregistrement,
            date: doc.data().date_enregistrement
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
          });
        })
      }
    )
  }

  voirDetail(item) {
    console.log(item.cmdCode);
    localStorage.cmdCode = item.cmdCode;
  }

}
