import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoriqueLivreurPageRoutingModule } from './historique-livreur-routing.module';

import { HistoriqueLivreurPage } from './historique-livreur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoriqueLivreurPageRoutingModule
  ],
  declarations: [HistoriqueLivreurPage]
})
export class HistoriqueLivreurPageModule {}
