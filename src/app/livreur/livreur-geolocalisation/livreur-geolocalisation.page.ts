import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Geolocation} from '@capacitor/core';
import * as L from 'leaflet';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsMapTypeId,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps';

import { ActionSheetController, Platform, AlertController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-livreur-geolocalisation',
  templateUrl: './livreur-geolocalisation.page.html',
  styleUrls: ['./livreur-geolocalisation.page.scss'],
})
export class LivreurGeolocalisationPage implements OnInit {
<<<<<<< HEAD
<<<<<<< HEAD
  map: any;
=======
  map: GoogleMap;
>>>>>>> 3b82dec (qr code)
  livreur_long: number;
  livreur_lat: number;
  lat_vendeur: string;
  long_vendeur: string;
  lat_acheteur: string;
  long_acheteur: string;
  numero_cmd: string;

  constructor(private firestore: AngularFirestore) { 
    this.lat_vendeur  = localStorage.getItem('lat_vendeur');
    this.long_vendeur = localStorage.getItem('long_vendeur');
    this.lat_acheteur = localStorage.getItem('lat_acheteur');
    this.long_acheteur = localStorage.getItem('long_acheteur');
    this.numero_cmd = localStorage.getItem('numero_cmd');
  }

  ngOnInit() {
<<<<<<< HEAD
=======
  map: GoogleMap;
  livreur_long: number;
  livreur_lat: number;
  lat_vendeur: string;
  long_vendeur: string;
  lat_acheteur: string;
  long_acheteur: string;

  constructor() { 
    this.lat_vendeur  = localStorage.getItem('lat_vendeur');
    this.long_vendeur = localStorage.getItem('long_vendeur');
    this.lat_acheteur = localStorage.getItem('lat_acheteur');
    this.long_acheteur = localStorage.getItem('long_acheteur');
  }

  ngOnInit() {
    this.getPosition();
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
  this.loadMap();
=======
   this.getPosition();
   this.loadMap();
>>>>>>> 3b82dec (qr code)
  }

  

  loadMap() {
<<<<<<< HEAD
    Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyDz7T3Ms81BUuEY0gETp4rrpEsRY1x3T74',
      API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyDz7T3Ms81BUuEY0gETp4rrpEsRY1x3T74'
    });
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
<<<<<<< HEAD
          lat: 6.3724685,
          lng: 2.326137
        },
        zoom: 14,
=======
          lat: 6.3906633,
          lng: 2.3941066,
        },
        zoom: 18,
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
        tilt: 30
=======
    // verification du niveau de la commande
    this.firestore.firestore.collection('infoCommande').where('cmdCode', '==', this.numero_cmd).onSnapshot(
      (querySnapchot) => {
        var niveau;
        querySnapchot.forEach((doc) => {
          if(doc.data().en_livraison === 'ok' && doc.data().remise === '') {
            niveau = 'vendeur';
          }
          if(doc.data().en_livraison === 'ok' && doc.data().remise === 'ok') {
            niveau = 'acheteur';
          }
        });

        //positionnement du marqueur en fonction du niveau de la commmande
        if(niveau === 'vendeur') {
          Environment.setEnv({
            API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyDz7T3Ms81BUuEY0gETp4rrpEsRY1x3T74',
            API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyDz7T3Ms81BUuEY0gETp4rrpEsRY1x3T74'
          });
          this.map = GoogleMaps.create('map_canvas', {
            camera: {
              target: {
                lat: this.lat_vendeur,
                lng: this.long_vendeur,
              },
              zoom: 18,
              tilt: 30
            }
          });
      
          let marker: Marker = this.map.addMarkerSync({
            title: 'vendeur',
            icon: 'blue',
            animation: 'DROP',
            position: {
              lat: +this.lat_vendeur,
              lng: +this.long_vendeur
            }
          });
        }
        if(niveau === 'acheteur') {
          Environment.setEnv({
            API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyDz7T3Ms81BUuEY0gETp4rrpEsRY1x3T74',
            API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyDz7T3Ms81BUuEY0gETp4rrpEsRY1x3T74'
          });
          this.map = GoogleMaps.create('map_canvas', {
            camera: {
              target: {
                lat: this.lat_acheteur,
                lng: this.long_acheteur,
              },
              zoom: 18,
              tilt: 30
            }
          });
      
          let marker: Marker = this.map.addMarkerSync({
            title: 'Client',
            icon: 'blue',
            animation: 'DROP',
            position: {
              lat: +this.lat_acheteur,
              lng: +this.long_acheteur
            }
          });
        }
>>>>>>> 3b82dec (qr code)
      }
    )
   
    // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
    //   alert('clicked');
    // });
    
  

<<<<<<< HEAD
=======
    let marker: Marker = this.map.addMarkerSync({
      title: 'Ionic',
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: 6.3906633,
        lng: 2.3941066
      }
    });
    // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
    //   alert('clicked');
    // });
    
  

>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
  
   }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    console.log(position.coords.latitude, position.coords.longitude);
    this.livreur_lat = position.coords.latitude;
    this.livreur_long = position.coords.longitude;
  }
  

  watchPosition() {
    const wait = Geolocation.watchPosition({}, (position, err) => {
      this.livreur_lat = position.coords.latitude;
      this.livreur_long = position.coords.longitude;
    });
    console.log(this.livreur_lat, this.livreur_long);
  }

}
