import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/app/authentification/authentification.service';
import { Geolocation} from '@capacitor/core';

import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from '@capacitor/core';
import { ActionSheetController, AlertController } from '@ionic/angular';

const { PushNotifications, Modals } = Plugins;

@Component({
  selector: 'app-inscription-livreur',
  templateUrl: './inscription-livreur.page.html',
  styleUrls: ['./inscription-livreur.page.scss'],
})
export class InscriptionLivreurPage implements OnInit {
  dataLivreur = {
    nom: '',
    prenom: '',
    numero: '',
    email: '',
    num_carte: '',
    password: '',
    password2: '',
    moyen_livraison: '',
    type: 'livreur',
<<<<<<< HEAD
<<<<<<< HEAD
    en_livraison: false,
=======
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
    statut: 'inactif'
=======
    statut: 'inactif',
    societe_livraison: '',
    code_validation: '',
    compte_valider: 'non'
>>>>>>> 3b82dec (qr code)
  };
  errorMessage: any;
  type_livreur: string;

  constructor(private authService: AuthentificationService,
              private router: Router,
              public alertController: AlertController,
              public actionSheetController: ActionSheetController,
              private firestore: AngularFirestore) { }

  ngOnInit() {
  }

  submit() {
    if (this.dataLivreur.password === this.dataLivreur.password2) {
      //si le livreur est indépandant
      if(this.dataLivreur.societe_livraison === '') {
        const email = this.dataLivreur.email;
        const password = this.dataLivreur.password;
        this.authService.signUpUser(email, password).then(
          () => {
            localStorage.pseudo = this.dataLivreur.nom;
            localStorage.email = this.dataLivreur.email;
            localStorage.type = 'livreur';
            this.generateTokenFCM();
            this.firestore.collection('livreur').add(this.dataLivreur);
            this.firestore.collection('user').add(this.dataLivreur);
            this.firestore.collection('portefeuille').add({
              proprietaire: this.dataLivreur.email,
              solde: 0,
              retrait: 0,
              ajout: 0
            });
            this.router.navigate(['livreur-tabs']);
          },
          (error) => {
            this.errorMessage = error;
          }
        );
      }
      //si le livreur est affilier
      if(this.dataLivreur.societe_livraison !== '') {
        //on lui genere un code de validation de compte à communiquer à sa societe de livraison
        var code_validation = this.getRandomIntInclusive(100000, 900000);
        this.dataLivreur.code_validation = code_validation;
        this.firestore.collection('livreur_affilier').add(this.dataLivreur).then(
          (result)=> {
            localStorage.code_validation = code_validation;
            localStorage.pseudo = this.dataLivreur.nom;
            localStorage.email = this.dataLivreur.email;
            localStorage.type = 'livreur';
            this.generateTokenFCM();
            this.router.navigate(['livreur-affilier-code']);
          }
        );
      }
     
    }
  }

  async getCurrentPosition() {
    const coordinates = await Geolocation.getCurrentPosition();
    console.log('Current', coordinates);
  }

  watchPosition() {
    const wait = Geolocation.watchPosition({}, (position, err) => {
      console.log(position);

    });
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Categories de livreur',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Livreur indépendant',
        //icon: 'trash',
        handler: () => {
          console.log('independant clicked');
          this.type_livreur = 'independant';
        }
      }, {
        text: 'Société de livraison',
       // icon: 'share',
        handler: () => {
          console.log('affilier clicked');
          this.type_livreur = 'affilier';
        }
      }, 
         {
        text: 'cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


  generateTokenFCM() {
    // Request permission to use push notifications
      // iOS will prompt user and return if they granted permission or not
      // Android will just grant without prompting
      PushNotifications.requestPermission().then(result => {
        if (result.granted) {
          // Register with Apple / Google to receive push via APNS/FCM
          PushNotifications.register();
        } else {
          // Show some error
        }
      });
  
      PushNotifications.addListener(
        'registration',
        (token: PushNotificationToken) => {
          alert('Push registration success, token: ' + token.value);
          this.firestore.collection('testtoken').add({
            userid: this.dataLivreur.email,
            usertoken: token.value, 
          })
        },
      );
  
      PushNotifications.addListener('registrationError', (error: any) => {
        alert('Error on registration: ' + JSON.stringify(error));
      });
  
      PushNotifications.addListener(
        'pushNotificationReceived',
        (notification: PushNotification) => {
          // alert('Push received: ' + JSON.stringify(notification));
  
          // tslint:disable-next-line:prefer-const
          let alert =  Modals.alert ( {
            title: notification.title,
            message: notification.body
          });
        },
      );
  
      PushNotifications.addListener(
        'pushNotificationActionPerformed',
        (notification: PushNotificationActionPerformed) => {
          alert('Push action performed: ' + JSON.stringify(notification));
        },
      );
    }


    getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    


}
