import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-portfeuil-livreur',
  templateUrl: './portfeuil-livreur.page.html',
  styleUrls: ['./portfeuil-livreur.page.scss'],
})
export class PortfeuilLivreurPage implements OnInit {
  email: string;
  solde: any;
  transactions: any[];
  trans_date: any[];

  constructor(private firestore: AngularFirestore) { }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }

  
  ngOnInit() {
    this.email = localStorage.getItem('email');
    console.log(this.email)

    this.firestore.firestore.collection('portefeuille').where('proprietaire', '==', this.email).onSnapshot(
      (querySnapchot)=> {
        querySnapchot.forEach((doc) => {
          this.solde = doc.data().solde;
          console.log('solde:'+this.solde);
        })
      }
    )

    this.toute();
  }


   toute() {
    this.firestore.firestore.collection('transactions').where('id_user', '==', this.email).onSnapshot(
      (querySnapchot) => {
       this.transactions = [];
       this.trans_date = [];
        querySnapchot.forEach((doc) => {
          this.transactions.push({
            date: doc.data().date_transaction,
            heure: doc.data().heure_transaction,
            motif: doc.data().motif,
            pour_commande: doc.data().pour_commande,
            solde: doc.data().solde,
            montant: doc.data().montant
          });

          this.trans_date.push(doc.data().date_transaction);
          this.trans_date = Array.from(new Set(this.trans_date));
          console.log(this.trans_date);

          console.log('transactions:', this.transactions);


        })

      }
    )
  }

  recharge() {
    this.firestore.firestore.collection('transactions').where('id_user', '==', this.email).where('motif', '==', 'recharge').onSnapshot(
      (querySnapchot) => {
       this.transactions = [];
       this.trans_date = [];
        querySnapchot.forEach((doc) => {
          this.transactions.push({
            date: doc.data().date_transaction,
            heure: doc.data().heure_transaction,
            motif: doc.data().motif,
            pour_commande: doc.data().pour_commande,
            solde: doc.data().solde,
            montant: doc.data().montant
          });

          this.trans_date.push(doc.data().date_transaction);
          this.trans_date = Array.from(new Set(this.trans_date));
          console.log(this.trans_date);
 

          console.log('transactions:', this.transactions);

        })

      }
    )
  }

  paiement() {
    this.firestore.firestore.collection('transactions').where('id_user', '==', this.email).where('motif', '==', 'paiement').onSnapshot(
      (querySnapchot) => {
       this.transactions = [];
       this.trans_date = [];
        querySnapchot.forEach((doc) => {
          this.transactions.push({
            date: doc.data().date_transaction,
            heure: doc.data().heure_transaction,
            motif: doc.data().motif,
            pour_commande: doc.data().pour_commande,
            solde: doc.data().solde,
            montant: doc.data().montant
          });

          this.trans_date.push(doc.data().date_transaction);
          this.trans_date = Array.from(new Set(this.trans_date));
          console.log(this.trans_date);

          console.log('transactions:', this.transactions);

        })

      }
    )
  }


}
