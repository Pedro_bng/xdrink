import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddDepotPageRoutingModule } from './add-depot-routing.module';

import { AddDepotPage } from './add-depot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddDepotPageRoutingModule
  ],
  declarations: [AddDepotPage]
})
export class AddDepotPageModule {}
