import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddDepotPage } from './add-depot.page';

describe('AddDepotPage', () => {
  let component: AddDepotPage;
  let fixture: ComponentFixture<AddDepotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDepotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddDepotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
