import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';


@Component({
  selector: 'app-historique-vendeur',
  templateUrl: './historique-vendeur.page.html',
  styleUrls: ['./historique-vendeur.page.scss'],
})
export class HistoriqueVendeurPage implements OnInit {
  nom_stock: string;
  historique_vente: any[];
  email: string;
  historique_achat: any[];
  statut: '';

  constructor(private firestore: AngularFirestore,
              private router: Router  ) { 
                this.nom_stock = localStorage.getItem('stock_vendeur');
                this.email = localStorage.getItem('email');
               
              }

  ngOnInit() {
    this.historique();
  }

  historique () {
    this.firestore.firestore.collection('infoCommande').where('vendeur_choisi', '==', this.nom_stock ).onSnapshot(
      (querySnapchot) => {
        this.historique_vente = [];
<<<<<<< HEAD
        querySnapchot.forEach((doc) => {
          this.historique_vente.push({
            cmdCode: doc.data().cmdCode,
            statut: doc.data().statut
=======
        let enregistrer;
        let en_attente;
        let en_livraison;
        let remise;
        let livrer;
        let statut;
        querySnapchot.forEach((doc) => {
            enregistrer = doc.data().enregistrer,
            en_attente = doc.data().en_attente,
            en_livraison = doc.data().en_livraison,
            remise = doc.data().remise,
            livrer = doc.data().livrer
            if(enregistrer === 'ok' && en_attente === '' && en_livraison === '' && remise === '' && livrer === ''){
              statut = 'enregistrer'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === '' && remise === '' && livrer === ''){
              statut = 'en_attente'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === '' && livrer === ''){
              statut = 'en_livraison'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === 'ok' && livrer === ''){
              statut = 'remise'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === 'ok' && livrer === 'ok'){
              statut = 'livrer'
            }

          this.historique_vente.push({
            cmdCode: doc.data().cmdCode,
            statut: statut,
            heure: doc.data().heure_enregistrement,
            date: doc.data().date_enregistrement
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
          });

        })
        console.log(this.historique_vente);
      }
    )

    this.firestore.firestore.collection('infoCommande').where('email_acheteur', '==', this.email).onSnapshot(
      (querySnapchot) => {
        this.historique_achat = [];
<<<<<<< HEAD
        querySnapchot.forEach((doc) => {
          this.historique_achat.push({
            cmdCode: doc.data().cmdCode,
            statut: doc.data().statut
=======
        let enregistrer;
        let en_attente;
        let en_livraison;
        let remise;
        let livrer;
        let statut;
        querySnapchot.forEach((doc) => {
          enregistrer = doc.data().enregistrer,
            en_attente = doc.data().en_attente,
            en_livraison = doc.data().en_livraison,
            remise = doc.data().remise,
            livrer = doc.data().livrer
            if(enregistrer === 'ok' && en_attente === '' && en_livraison === '' && remise === '' && livrer === ''){
              statut = 'enregistrer'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === '' && remise === '' && livrer === ''){
              statut = 'en_attente'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === '' && livrer === ''){
              statut = 'en_livraison'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === 'ok' && livrer === ''){
              statut = 'remise'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === 'ok' && livrer === 'ok'){
              statut = 'livrer'
            }
          this.historique_achat.push({
            cmdCode: doc.data().cmdCode,
            statut: statut,
            heure: doc.data().heure_enregistrement,
            date: doc.data().date_enregistrement
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
          });
        })

        console.log(this.historique_achat);
      }
    )




  }

  voirDetail(item) {
    console.log(item.cmdCode);
    localStorage.cmdCode = item.cmdCode;
  }

}
