import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthentificationService } from 'src/app/authentification/authentification.service';
import { Geolocation} from '@capacitor/core';

import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from '@capacitor/core';

const { PushNotifications, Modals } = Plugins;


// tslint:disable-next-line:class-name
interface Nom_prods {
  art: string;
}
@Component({
  selector: 'app-depot-marchand',
  templateUrl: './depot-marchand.page.html',
  styleUrls: ['./depot-marchand.page.scss'],
})
export class DepotMarchandPage implements OnInit {
  // tslint:disable-next-line:variable-name
  nom_prods = [];
  dataStock =  {
    qty: '0',
    prix_unitaire: 0,
    unite: '',
    nom_prod: '',
    nom_depot: '',
    latitude: 0,
    longitude: 0,
    localisation_detail1: '',
    localisation_detail2: ''
  };
  dataDepot = {
    nom_depot: '',
    nom: '',
    email: '',
    telephone_gerant: '',
    ifu: '',
    adresse_livraison: '',
    password: '',
    password2: '',
    type: 'vendeur',
    latitude: 0,
    longitude: 0,
    localisation_detail1: '',
    localisation_detail2: '',
    precision: 'depot_marchand'
  };

  errorMessage: any;

  latitude: number;
  longitude: number;

  constructor(private router: Router,
              private authService: AuthentificationService,
              private firestore: AngularFirestore,
              public alertController: AlertController)
              {
                /*if (localStorage.getItem('nom_prods')){
                  this.nom_prods = JSON.parse(localStorage.getItem('nom_prods'));
                }*/
                /*this.nom_prods = JSON.parse(localStorage.getItem('nom_prods')) || [];
                console.log(this.nom_prods);*/

                //recuperation du nom des produits pour le stock
                this.firestore.collection('sobebra').snapshotChanges()
                .subscribe(sob => {
                  this.nom_prods = [];
                  sob.forEach(sobx => {
                    this.nom_prods.push(sobx.payload.doc.data()['nom_produit']);
                  })
                  console.log(this.nom_prods);
                })

              }

  ngOnInit() {
  }

  soumettre() {
    console.log(this.dataDepot.localisation_detail1, this.dataDepot.localisation_detail2);
    if (this.dataDepot.password === this.dataDepot.password2) {
     if(this.dataDepot.latitude !== 0 && this.dataDepot.longitude !== 0) {

      const email = this.dataDepot.email;
      const password = this.dataDepot.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataDepot.nom;
          localStorage.email = this.dataDepot.email;
          localStorage.type = 'vendeur';
          this.generateTokenFCM();
          localStorage.stock_vendeur = this.dataDepot.nom_depot;
          this.firestore.collection('vendeur').add(this.dataDepot);
          this.firestore.collection('user').add(this.dataDepot);
          this.firestore.collection('portefeuille').add({
            proprietaire: this.dataDepot.email,
            solde: 0,
            retrait: 0,
            ajout: 0
          });

          //creation du stock pour le vendeur
          this.nom_prods.forEach(element => {
            console.log(element);
            this.dataStock.nom_prod = element;
            this.dataStock.nom_depot = this.dataDepot.nom_depot;
            this.dataStock.localisation_detail1 = this.dataDepot.localisation_detail1;
            this.dataStock.localisation_detail2 = this.dataDepot.localisation_detail2;
            this.firestore.collection(this.dataDepot.nom_depot).doc(element).set(this.dataStock);
          });
            // identification du depot qui commande
          this.firestore.firestore.collection('vendeur').where('email', '==', email).onSnapshot(
            (querySnapshot) => {
             querySnapshot.forEach((doc) => {
                 localStorage.nom_dep = doc.data().nom_depot;
                 localStorage.latitude = doc.data().latitude;
                 localStorage.longitude = doc.data().longitude;
               });
           });
          this.router.navigate(['vendeur-tabs']);
        },
        (error) => {
          this.errorMessage = error;
        }
      );
      } else { this.presentAlertConfirm2(); }
    }
  }

  async presentAlertConfirm2() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Activez votre localisation et ressayez</strong>!!!',
      buttons: [
         {
          text: 'ok',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }



  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Avant de confirmer la géolocalisation vous devez être dans votre depot</strong>!!!',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmer position',
          handler: () => {
            console.log('Confirm Okay');
            this.getPosition();
          }
        }
      ]
    });

    await alert.present();
  }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    this.dataDepot.latitude = position.coords.latitude;
    this.dataDepot.longitude = position.coords.longitude;
    this.dataStock.latitude = position.coords.latitude;
    this.dataStock.longitude = position.coords.longitude;
    console.log(this.dataDepot.latitude, this.dataDepot.longitude);
  }

  generateTokenFCM() {
    // Request permission to use push notifications
      // iOS will prompt user and return if they granted permission or not
      // Android will just grant without prompting
      PushNotifications.requestPermission().then(result => {
        if (result.granted) {
          // Register with Apple / Google to receive push via APNS/FCM
          PushNotifications.register();
        } else {
          // Show some error
        }
      });
  
      PushNotifications.addListener(
        'registration',
        (token: PushNotificationToken) => {
          alert('Push registration success, token: ' + token.value);
          this.firestore.collection('testtoken').add({
            userid: this.dataDepot.email,
            usertoken: token.value, 
            vendeur: this.dataDepot.nom_depot
          })
        },
      );
  
      PushNotifications.addListener('registrationError', (error: any) => {
        alert('Error on registration: ' + JSON.stringify(error));
      });
  
      PushNotifications.addListener(
        'pushNotificationReceived',
        (notification: PushNotification) => {
          // alert('Push received: ' + JSON.stringify(notification));
  
          // tslint:disable-next-line:prefer-const
          let alert =  Modals.alert ( {
            title: notification.title,
            message: notification.body
          });
        },
      );
  
      PushNotifications.addListener(
        'pushNotificationActionPerformed',
        (notification: PushNotificationActionPerformed) => {
          alert('Push action performed: ' + JSON.stringify(notification));
        },
      );
    }

}
