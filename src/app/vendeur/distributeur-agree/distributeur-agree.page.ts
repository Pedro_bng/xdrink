import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { Geolocation} from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { AuthentificationService } from 'src/app/authentification/authentification.service';


import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from '@capacitor/core';

const { PushNotifications, Modals } = Plugins;


@Component({
  selector: 'app-distributeur-agree',
  templateUrl: './distributeur-agree.page.html',
  styleUrls: ['./distributeur-agree.page.scss'],
})
export class DistributeurAgreePage implements OnInit {

   // tslint:disable-next-line:variable-name
   nom_prods = [];
   dataStock =  {
     qty: '0',
     prix_unitaire: 0,
     unite: '',
     nom_prod: '',
     nom_depot: '',
     latitude: 0,
     longitude: 0,
     localisation_detail1: '',
     localisation_detail2: ''
   };

  dataDistributeur = {
    type_distributeur: '',
    distributeur_agreer: '',
    email: '',
    password: '',
    password2: '',
    type: 'vendeur',
    secretCode: '',
    latitude: 0,
    longitude: 0,
    localisation_detail1: '',
    localisation_detail2: '',
    precision: 'distributeur_agreer'
  };

  // tslint:disable-next-line:variable-name
  type_distributeur: any;

  errorMessage: any;

  code: any;

  verificationCode = false;

  constructor(private router: Router,
              private authService: AuthentificationService,
              private firestore: AngularFirestore,
              public alertController: AlertController,
              public actionSheetController: ActionSheetController) {

               /* this.nom_prods = JSON.parse(localStorage.getItem('nom_prods')) || [];
                console.log(this.nom_prods);*/

                 //recuperation du nom des produits pour le stock
                 this.firestore.collection('sobebra').snapshotChanges()
                 .subscribe(sob => {
                   sob.forEach(sobx => {
                     this.nom_prods.push(sobx.payload.doc.data()['nom_produit']);
                   })
                   console.log(this.nom_prods);
                 })
              }

  ngOnInit() {
  }

  soumettre2() {
    // tslint:disable-next-line:max-line-length
    this.firestore.firestore.collection('code-confirmation-distributeur-agreer').where('nom', '==', this.dataDistributeur.distributeur_agreer).onSnapshot(
      (querySnapshot) => {
       querySnapshot.forEach((doc) => {
           this.code = doc.data().code;
         });
     });

    console.log(this.code);
    if (this.code === this.dataDistributeur.secretCode) {
      if (this.dataDistributeur.password === this.dataDistributeur.password2 ) {
        if (this.dataDistributeur.latitude !== 0 && this.dataDistributeur.longitude !== 0) {
        const email = this.dataDistributeur.email;
        const password = this.dataDistributeur.password;
        this.authService.signUpUser(email, password).then(
          () => {
            // localStorage.pseudo = this.dataDistributeur.nom;
            localStorage.email = this.dataDistributeur.email;
            localStorage.type = 'vendeur';
            localStorage.stock_vendeur = this.dataDistributeur.distributeur_agreer;
            this.generateTokenFCM();
            this.firestore.collection('vendeur').add(this.dataDistributeur);
            this.firestore.collection('user').add(this.dataDistributeur);
            this.firestore.collection('portefeuille').add({
              proprietaire: this.dataDistributeur.email,
              solde: 0,
              retrait: 0,
              ajout: 0
            });

            // creation du stock pour le vendeur
            this.nom_prods.forEach(element => {
            console.log(element);
            this.dataStock.nom_prod = element;
            this.dataStock.nom_depot = this.dataDistributeur.distributeur_agreer;
            this.dataStock.localisation_detail1 = this.dataDistributeur.localisation_detail1;
            this.dataStock.localisation_detail2 = this.dataDistributeur.localisation_detail2;
            this.firestore.collection(this.dataDistributeur.distributeur_agreer).doc(element).set(this.dataStock);
          });

            this.firestore.firestore.collection('vendeur').where('email', '==', email).onSnapshot(
            (querySnapshot) => {
             querySnapshot.forEach((doc) => {
                 localStorage.nom_distributeur = doc.data().distributeur_agreer;
                 localStorage.latitude = doc.data().latitude;
                 localStorage.longitude = doc.data().longitude;
               });
           });
            this.router.navigate(['vendeur-tabs']);
          }, (error) => {
            this.errorMessage = error;
          }
        );
        } else { this.presentAlertConfirm2(); } 
      }

     }else {
       this.verificationCode = true;
       // tslint:disable-next-line:prefer-const
       let message = 'code de verification incorrect! veuillez nous contacter';
     }
  }

  async presentAlertConfirm2() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Activez votre localisation et ressayez</strong>!!!',
      buttons: [
         {
          text: 'ok',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  soumettre() {
    if (this.dataDistributeur.password === this.dataDistributeur.password2) {
      const email = this.dataDistributeur.email;
      const password = this.dataDistributeur.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataDistributeur.distributeur_agreer;
          localStorage.email = this.dataDistributeur.email;
          localStorage.type = 'vendeur';
          this.firestore.collection('distributeur_agree').add(this.dataDistributeur);
          this.firestore.collection('user').add(this.dataDistributeur);
          this.router.navigate(['vendeur-tabs']);
        },
        (error) => {
          this.errorMessage = error;
        }
      );
    }
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Types de distributeur',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Distributeur agreer SOBEBRA',
        icon: 'trash',
        handler: () => {
          console.log('sobebra clicked');
          this.type_distributeur = 'sobebra';
          this.dataDistributeur.type_distributeur = 'sobebra';
        }
      }, {
        text: 'Distributeur LIBS',
        icon: 'share',
        handler: () => {
          console.log('libs clicked');
          this.type_distributeur = 'libs';
          this.dataDistributeur.type_distributeur = 'libs'
        }
      }, {
        text: 'Grossiste LIQUEURS ET CHAMPAGNE',
        icon: 'caret-forward-circle',
        handler: () => {
          console.log('liqueur et champagne clicked');
          this.type_distributeur = 'liqueur et champagne';
          this.dataDistributeur.type_distributeur = 'liqueur_champagne'
        }
      }, {
        text: 'Grossiste JUS ET AUTRES',
        icon: 'heart',
        handler: () => {
          console.log('jus et autres clicked');
          this.type_distributeur = 'jus et autres';
          this.dataDistributeur.type_distributeur = 'jus_autres'
        }
      }, {
        text: 'Autres GROSSISTE',
        icon: 'heart',
        handler: () => {
          console.log('autres grossistes clicked');
          this.type_distributeur = 'atres grossistes';
          this.dataDistributeur.type_distributeur = 'autres_grossistes'
        }
      },
         {
        text: 'cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Avant de confirmer la géolocalisation vous devez être dans votre depot principal</strong>!!!',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmer position',
          handler: () => {
            console.log('Confirm Okay');
            this.getPosition();
          }
        }
      ]
    });

    await alert.present();
  }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    this.dataDistributeur.latitude = position.coords.latitude;
    this.dataDistributeur.longitude = position.coords.longitude;
   // this.dataStock.latitude = position.coords.latitude;
   // this.dataStock.longitude = position.coords.longitude;
    console.log(this.dataDistributeur.latitude, this.dataDistributeur.longitude);
  }

  generateTokenFCM() {
    // Request permission to use push notifications
      // iOS will prompt user and return if they granted permission or not
      // Android will just grant without prompting
      PushNotifications.requestPermission().then(result => {
        if (result.granted) {
          // Register with Apple / Google to receive push via APNS/FCM
          PushNotifications.register();
        } else {
          // Show some error
        }
      });
  
      PushNotifications.addListener(
        'registration',
        (token: PushNotificationToken) => {
          alert('Push registration success, token: ' + token.value);
          this.firestore.collection('testtoken').add({
            userid: this.dataDistributeur.email,
            usertoken: token.value, 
            vendeur: this.dataDistributeur.distributeur_agreer
          })
        },
      );
  
      PushNotifications.addListener('registrationError', (error: any) => {
        alert('Error on registration: ' + JSON.stringify(error));
      });
  
      PushNotifications.addListener(
        'pushNotificationReceived',
        (notification: PushNotification) => {
          // alert('Push received: ' + JSON.stringify(notification));
  
          // tslint:disable-next-line:prefer-const
          let alert =  Modals.alert ( {
            title: notification.title,
            message: notification.body
          });
        },
      );
  
      PushNotifications.addListener(
        'pushNotificationActionPerformed',
        (notification: PushNotificationActionPerformed) => {
          alert('Push action performed: ' + JSON.stringify(notification));
        },
      );
    }

}
