import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModifierProduitPanierPage } from 'src/app/modals/modifier-produit-panier/modifier-produit-panier.page';
<<<<<<< HEAD
=======
interface Cart {
  id: number;
  img: string;
  name: string;
  qty: number;
  amount: number;
  qtymod: number;
  amountmod: number;
}
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd

@Component({
  selector: 'app-vendeur-panier-stock',
  templateUrl: './vendeur-panier-stock.page.html',
  styleUrls: ['./vendeur-panier-stock.page.scss'],
})
export class VendeurPanierStockPage implements OnInit {
<<<<<<< HEAD

  // tslint:disable-next-line:variable-name
  produit_panier = [];
  cartStock = [];
  // tslint:disable-next-line:variable-name
  nom_prod: any;
  constructor(private modalController: ModalController,
              private router: Router,
              private firestore: AngularFirestore) { }
=======
  data = [];
  // tslint:disable-next-line:variable-name
  produit_panier: Cart[] = [];
  cartStock = [];
  // tslint:disable-next-line:variable-name
  nom_prod: any;
  inputQty = false;
  inputAmount = false;
  option: string;
  visual: string;
  stock: string;
  constructor(private modalController: ModalController,
              private router: Router,
              private firestore: AngularFirestore) {
                this.stock = localStorage.getItem('stock_vendeur');
                console.log(this.stock);

                this.firestore.collection(this.stock).snapshotChanges()
                .subscribe(produit => {
                  this.data = [];
                  produit.forEach(produitx => {
                   // console.log ('image:' + produitx.payload.doc.data()['qty'] );
                    this.data.push({
                      name: produitx.payload.doc.data()['nom_prod'],
                      qty: produitx.payload.doc.data()['qty']
                    })
                  })
                  console.log(this.data);
                })
                
               }
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd

  ngOnInit() {
    if (localStorage.getItem('produit-stock')){
      this.produit_panier = JSON.parse(localStorage.getItem('produit-stock'));
<<<<<<< HEAD
    }
=======

     
      this.option = localStorage.getItem('option');
                if (this.option === 'achat') {
                  this.visual = 'Quantité acheté';
                }
                if (this.option === 'vente') {
                  this.visual = 'Quantité vendu';
                }
    }
    
   
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
  }

  openModal(item){
    this.presentModal();
    this.cartStock.push(item);
    this.nom_prod = item.name;
    localStorage.nom_prod = item.name;
    console.log(this.nom_prod);
    localStorage.setItem('stockref', JSON.stringify(this.cartStock));
    // tslint:disable-next-line:prefer-const
    let exist = false;

    this.cartStock.forEach(element => {
      if (element.id === item.id){
        exist = true;
        this.cartStock.pop();
      }
    });
  }

<<<<<<< HEAD
=======
  increase(item){
    if (item.qty){
      item.qty = item.qty + 1;
    }
    else{
      item.qty = 2;
    }
  }
  decrease(item){
    if (!(item.qty - 1 <= 0)){
      item.qty = item.qty - 1;
    }

  }

>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
  delete(item){
    this.produit_panier.forEach((element, i) => {
      if (element.id === item.id){
        console.log('i :>> ', i);
        this.produit_panier.splice(i, 1);
      }
    });
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: ModifierProduitPanierPage,
      cssClass: 'livraison-modal',
      componentProps: {
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    console.log(data);
  }

<<<<<<< HEAD
  actualiser() {
    this.router.navigateByUrl('vendeur-tabs/stock');
  }

}
=======
  actualiser(item:Cart) {

    
    //pour mettre à jour les quantiter
    if (item.qtymod !== 0 && item.qtymod !== null ) {
      console.log("ok");
      if(this.option === 'achat') {
        console.log('achat');
        console.log(item.name);
        console.log(item.qty);
        console.log(item.qtymod);
        item.qty = item.qty + item.qtymod
        console.log('somme:'+item.qty)
        this.firestore.collection(this.stock).doc(item.name).update({qty:item.qty});
      }
      if(this.option === 'vente') {
        console.log('vente');
        console.log(item.name);
        console.log(item.qty);
        console.log(item.qtymod);
        item.qty = item.qty - item.qtymod
        console.log('moins:'+item.qty)
        this.firestore.collection(this.stock).doc(item.name).update({qty:item.qty});
      }
    }

    //pour modifier les prix
    if(item.amountmod !== 0 && item.amountmod !== null) {
      if(this.option === 'prix') {
        this.firestore.collection(this.stock).doc(item.name).update({prix_unitaire:item.amountmod})
      }
    }

    this.produit_panier.forEach((element, i) => {
      if (element.id === item.id){
        console.log('i :>> ', i);
        this.produit_panier.splice(i, 1);
        console.log(this.produit_panier.length);
      }
    });

    if(this.produit_panier.length === 0) {
      this.router.navigateByUrl('vendeur-tabs/stock');
    }
    
   

  }

  

}


/*this.produit_panier.forEach(produit => {
  if (produit.name = item.name) {
    console.log(this.produit_panier)
   // console.log(this.produit_panier);
    //console.log(item.qty);
    this.data.forEach(datax => {
      if(datax.name === item.name ) {
        console.log('ajout')
        if (item.qty !== undefined) {
          if (this.option === 'achat') {
            datax.qty = datax.qty + item.qty;
            console.log(datax.qty);
           // this.firestore.collection(this.stock).doc(this.nom_prod).update(this.qtyAct);
          }
    
          if (this.option === 'vente') {
            datax.qty = datax.qty - item.qty;
            console.log(datax.qty);
           // this.firestore.collection(this.stock).doc(this.nom_prod).update(this.qtyAct);
          }
        }
      }
    })
  }
})*/
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
