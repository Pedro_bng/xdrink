import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthentificationService } from '../authentification/authentification.service';

import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from '@capacitor/core';

const { PushNotifications, Modals } = Plugins;

@Component({
  selector: 'app-inscription-particulier-client',
  templateUrl: './inscription-particulier-client.page.html',
  styleUrls: ['./inscription-particulier-client.page.scss'],
})
export class InscriptionParticulierClientPage implements OnInit {

  dataParticulier = {
    nom: '',
    prenoms: '',
<<<<<<< HEAD
<<<<<<< HEAD
    telephone: '',
=======
    telephone_gerant: '',
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
=======
    telephone_gerant: '',
>>>>>>> 3b82dec (qr code)
    email: '',
    password: '',
    password2: '',
    type: 'client',
    precision: 'particulier'
  };
  errorMessage: any;

  constructor(private router: Router,
              private authService: AuthentificationService,
              private firestore: AngularFirestore) { }

  ngOnInit() {
  }

  soumettre() {
    if (this.dataParticulier.password === this.dataParticulier.password2) {
      const email = this.dataParticulier.email;
      const password = this.dataParticulier.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataParticulier.nom;
          localStorage.email = this.dataParticulier.email;
          localStorage.type = 'client';
          this.generateTokenFCM()
          this.firestore.collection('client').add(this.dataParticulier);
          this.firestore.collection('user').add(this.dataParticulier);
          this.firestore.collection('portefeuille').add({
            proprietaire: this.dataParticulier.email,
            solde: 0,
            retrait: 0,
            ajout: 0
          })
          this.router.navigate(['tabs']);
        },
        (error) => {
          this.errorMessage = error;
        }
      );
    }
  }


  generateTokenFCM() {
    // Request permission to use push notifications
      // iOS will prompt user and return if they granted permission or not
      // Android will just grant without prompting
      PushNotifications.requestPermission().then(result => {
        if (result.granted) {
          // Register with Apple / Google to receive push via APNS/FCM
          PushNotifications.register();
        } else {
          // Show some error
        }
      });
  
      PushNotifications.addListener(
        'registration',
        (token: PushNotificationToken) => {
          alert('Push registration success, token: ' + token.value);
          this.firestore.collection('testtoken').add({
            userid: this.dataParticulier.email,
            usertoken:token.value
          })
        },
      );
  
      PushNotifications.addListener('registrationError', (error: any) => {
        alert('Error on registration: ' + JSON.stringify(error));
      });
  
      PushNotifications.addListener(
        'pushNotificationReceived',
        (notification: PushNotification) => {
          // alert('Push received: ' + JSON.stringify(notification));
  
          // tslint:disable-next-line:prefer-const
          let alert =  Modals.alert ( {
            title: notification.title,
            message: notification.body
          });
        },
      );
  
      PushNotifications.addListener(
        'pushNotificationActionPerformed',
        (notification: PushNotificationActionPerformed) => {
          alert('Push action performed: ' + JSON.stringify(notification));
        },
      );
    }


  }
  


