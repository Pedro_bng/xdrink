import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController, AlertController } from '@ionic/angular';
import { LivraisonChoixPage } from '../modals/livraison-choix/livraison-choix.page';
import { MoyenDePaiementChoixPage } from '../modals/moyen-de-paiement-choix/moyen-de-paiement-choix.page';

@Component({
  selector: 'app-livraison-client',
  templateUrl: './livraison-client.page.html',
  styleUrls: ['./livraison-client.page.scss'],
})
export class LivraisonClientPage implements OnInit {
  statut: any;

  dataCommande = {
<<<<<<< HEAD
    statut: 'enregistrer',
=======
    heure_enregistrement: '',
    date_enregistrement: '',
    enregistrer: 'ok',
    en_attente: '',
    en_livraison: '',
    remise: '',
    livrer: '',
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
    cmdCode: '',
    prix_total: 0,
    date_livraison: '',
    heure_livraison: '',
    frais_livraison: 0,
    frais_total: 0,
    produit: '',
    qty: 0,
    amount: 0,
    vendeur_choisi: '',
<<<<<<< HEAD
=======
    precision_vendeur: '',
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
    moyen_livraison: '',
    email_acheteur: '',
    codeSecret: '',
    livraison_detail1: '',
    livraison_detail2: ''
   };

   qtyTotal: 0;

  livraisonAddress = {
    localisation: '',
    detail: ''
  };

  cart: any[];
  total = 0;
  // tslint:disable-next-line:variable-name
  moyen_de_paiement: any;
<<<<<<< HEAD
=======
  email: string;
  solde: any;
  solde_bloquer: number;
  solde_disponible: number;
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
  constructor(private modalController: ModalController,
              private alertController: AlertController,
              private firestore: AngularFirestore,
              private router: Router) {
    this.statut = localStorage.getItem('type');
<<<<<<< HEAD
   }

  ngOnInit() {
=======
    this.email = localStorage.getItem('email');
   }

  ngOnInit() {
    //on surveille le solde dans le porteufeuille
    //et on recupere le solde dans le porteufeuille
    this.firestore.firestore.collection('portefeuille').where('proprietaire', '==', this.email).onSnapshot(
      (querySnapchot) => {
        querySnapchot.forEach((doc) => {
          this.solde = doc.data().solde
        });
        console.log('solde:' +this.solde);
      }

    )

    
  
    this.firestore.firestore.collection('infoCommande').where('email_acheteur', '==', this.email ).where('livrer', '==', '').onSnapshot(
      (querySnapchot) => {
        this.solde_bloquer = 0 
        var frais_total = 0
        querySnapchot.forEach((doc) => {
          frais_total = doc.data().frais_total
          this.solde_bloquer = this.solde_bloquer + frais_total
        })
        console.log('solde_bloquer:' +this.solde_bloquer);

      }
    )

    var now = new Date();
    console.log(now);
    var annee   = now.getFullYear();
    var mois    = now.getMonth() + 1;
    var jour    = now.getDate();
    var heure   = now.getHours();
    var minute  = now.getMinutes();
    var seconde = now.getSeconds();
    console.log(annee,mois,jour);
    console.log(heure,minute,seconde);
    console.log(jour+"/"+mois+"/"+annee);
    console.log(heure+":"+minute+":"+seconde);
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
  }
  ionViewWillEnter(){
    this.cart = JSON.parse(localStorage.getItem('cart2')) || [];
    console.log('this.cart :>> ', this.cart);
    this.cart.forEach(element => {
      if (!element.amount){
        element.amount = 250;
      }
      if (!element.qty){
        element.qty = 1;
      }
    });
    this.total = 0;
    this.qtyTotal = 0;
    this.cart.forEach(element => {
      this.total = this.total + element.qty * element.amount;
      this.dataCommande.prix_total = this.total;
      this.dataCommande.frais_livraison = 0;
      this.dataCommande.frais_total = this.total + this.dataCommande.frais_livraison;
      this.qtyTotal = this.qtyTotal + element.qty;
    });
    console.log (this.qtyTotal);

    if (this.qtyTotal <= 3) {
      this.dataCommande.frais_livraison = 350;
      this.dataCommande.frais_total = this.total + this.dataCommande.frais_livraison;
      this.dataCommande.moyen_livraison = '2Roues';
    }
    if (this.qtyTotal > 4 && this.qtyTotal < 10) {
      this.dataCommande.frais_livraison = 700;
      this.dataCommande.frais_total = this.total + this.dataCommande.frais_livraison;
      this.dataCommande.moyen_livraison = '3Roues ';
    }
  }


  livraison(){
    console.log('livraison');
    this.presentModal();
  }


  paiement(){
    console.log('paiement');
    this.presentModalPaiement();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: LivraisonChoixPage,
      cssClass: 'livraison-modal',
      componentProps: {
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    this.livraisonAddress = data.livraison;
    console.log(data);
  }
  async presentModalPaiement() {
    const modal = await this.modalController.create({
      component: MoyenDePaiementChoixPage,
      cssClass: 'livraison-modal',
      componentProps: {
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    this.moyen_de_paiement = data.moyen_de_paiement;
    console.log(data);
  }

  commander(){
<<<<<<< HEAD
    this.dataCommande.livraison_detail1 = localStorage.getItem('livraison_localisation');
    this.dataCommande.livraison_detail2 = localStorage.getItem('livraison_detail');
    console.log(this.dataCommande.livraison_detail1, this.dataCommande.livraison_detail2);
    if (this.statut === 'client') {
      this.dataCommande.codeSecret = this.getRandomIntInclusive(100000, 900000);
      this.dataCommande.cmdCode = this.uuidv4();
      localStorage.qrcode = this.dataCommande.cmdCode;
      console.log(this.dataCommande.cmdCode);
      this.cart.forEach(element => {
        this.dataCommande.produit = element.name;
        this.dataCommande.qty = element.qty;
        this.dataCommande.amount = element.amount;
        this.dataCommande.vendeur_choisi = localStorage.getItem('depotFinal');
        this.dataCommande.email_acheteur = localStorage.getItem('email');
        console.log(this.dataCommande.vendeur_choisi);
        this.firestore.collection('commandes').add(this.dataCommande);
      });
=======
    console.log('solde:',this.solde);
    console.log('solde bloquer:', this.solde_bloquer);
    console.log('frais total:', this.dataCommande.frais_total);
    this.solde_disponible = this.solde - this.solde_bloquer;

    if(this.solde_disponible > this.dataCommande.frais_total) {

      var now = new Date();
      console.log(now);
      var annee   = now.getFullYear();
      var mois    = now.getMonth() + 1;
      var jour    = now.getDate();
      var heure   = now.getHours();
      var minute  = now.getMinutes();
      var seconde = now.getSeconds();

      this.dataCommande.date_enregistrement = jour+"/"+mois+"/"+annee;
      this.dataCommande.heure_enregistrement = heure+"h"+minute+"m"+seconde+'s';

      this.dataCommande.livraison_detail1 = localStorage.getItem('livraison_localisation');
      this.dataCommande.livraison_detail2 = localStorage.getItem('livraison_detail');
      console.log(this.dataCommande.livraison_detail1, this.dataCommande.livraison_detail2);
      if (this.statut === 'client') {
        this.dataCommande.codeSecret = this.getRandomIntInclusive(100000, 900000);
        this.dataCommande.cmdCode = this.uuidv4();
        console.log(this.dataCommande.cmdCode);
        this.cart.forEach(element => {
          this.dataCommande.produit = element.name;
          this.dataCommande.qty = element.qty;
          this.dataCommande.amount = element.amount;
          this.dataCommande.vendeur_choisi = localStorage.getItem('depotFinal');
          this.dataCommande.precision_vendeur = localStorage.getItem('precision_vendeur')
          this.dataCommande.email_acheteur = localStorage.getItem('email');
          console.log(this.dataCommande.vendeur_choisi);
          this.firestore.collection('commandes').add(this.dataCommande);
        });
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd

      //recuperation du token du vendeur choisi et envoi de notification
      this.firestore.firestore.collection('testtoken').where('vendeur', '==', this.dataCommande.vendeur_choisi).onSnapshot(
        (querySnapchot) => {
<<<<<<< HEAD
          querySnapchot.forEach((doc) => {
            this.firestore.collection('infoCommande').add({ livraison_detail1: this.dataCommande.livraison_detail1, livraison_detail2: this.dataCommande.livraison_detail2, codeSecret: this.dataCommande.codeSecret, cmdCode: this.dataCommande.cmdCode, vendeurToken: doc.data().usertoken, email_acheteur: this.dataCommande.email_acheteur, statut: this.dataCommande.statut, vendeur_choisi: 'en_attente_acceptation'})
          })
=======
          let tok_vendeur;
          querySnapchot.forEach((doc) => {
            tok_vendeur = doc.data().usertoken;
          })
          this.firestore.collection('infoCommande').add({ email_acheteur: this.dataCommande.email_acheteur, precision_vendeur:this.dataCommande.precision_vendeur, frais_total:this.dataCommande.frais_total, argent_vendeur:this.dataCommande.prix_total, argent_livreur:this.dataCommande.frais_livraison, date_enregistrement:this.dataCommande.date_enregistrement, heure_enregistrement:this.dataCommande.heure_enregistrement, enregistrer: this.dataCommande.enregistrer, en_attente: this.dataCommande.en_attente, en_livraison: this.dataCommande.en_livraison, remise: this.dataCommande.remise, livrer: this.dataCommande.livrer, livraison_detail1: this.dataCommande.livraison_detail1, livraison_detail2: this.dataCommande.livraison_detail2, codeSecret: this.dataCommande.codeSecret, cmdCode: this.dataCommande.cmdCode, vendeurToken: tok_vendeur, vendeur_choisi: 'en_attente_acceptation'})
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
        }
      )
      this.presentAlert();
      this.router.navigate(['tabs/accueil']);
      }

    if (this.statut === 'vendeur'){
<<<<<<< HEAD
      this.dataCommande.codeSecret = this.getRandomIntInclusive(100000, 900000);
      this.dataCommande.cmdCode = this.uuidv4();
      localStorage.qrcode = this.dataCommande.cmdCode;
      console.log(this.dataCommande.cmdCode);
      this.cart.forEach(element => {
        this.dataCommande.produit = element.name;
        this.dataCommande.qty = element.qty;
        this.dataCommande.amount = element.amount;
        this.dataCommande.vendeur_choisi = localStorage.getItem('depotFinal');
<<<<<<< HEAD
=======
        this.dataCommande.precision_vendeur = localStorage.getItem('precision_vendeur')
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
        this.dataCommande.email_acheteur = localStorage.getItem('email');
        console.log(this.dataCommande.vendeur_choisi);
        this.firestore.collection('commandes').add(this.dataCommande);
      });

       //recuperation du token du vendeur choisi et envoi de notification
       this.firestore.firestore.collection('testtoken').where('vendeur', '==', this.dataCommande.vendeur_choisi).onSnapshot(
        (querySnapchot) => {
<<<<<<< HEAD
          querySnapchot.forEach((doc) => {
            this.firestore.collection('infoCommande').add({ livraison_detail1:this.dataCommande.livraison_detail1, livraison_detail2: this.dataCommande.livraison_detail2, codeSecret: this.dataCommande.codeSecret, cmdCode: this.dataCommande.cmdCode, vendeurToken: doc.data().usertoken, email_acheteur: this.dataCommande.email_acheteur, statut: this.dataCommande.statut, vendeur_choisi: 'en_attente_acceptation'})
          })
=======
          let tok_vendeur;
          querySnapchot.forEach((doc) => {
            tok_vendeur = doc.data().usertoken;
          })
          this.firestore.collection('infoCommande').add({email_acheteur: this.dataCommande.email_acheteur,precision_vendeur:this.dataCommande.precision_vendeur, frais_total:this.dataCommande.frais_total, argent_vendeur:this.dataCommande.prix_total, argent_livreur:this.dataCommande.frais_livraison, date_enregistrement:this.dataCommande.date_enregistrement, heure_enregistrement:this.dataCommande.heure_enregistrement, enregistrer: this.dataCommande.enregistrer, en_attente: this.dataCommande.en_attente, en_livraison: this.dataCommande.en_livraison, remise: this.dataCommande.remise, livrer: this.dataCommande.livrer, livraison_detail1: this.dataCommande.livraison_detail1, livraison_detail2: this.dataCommande.livraison_detail2, codeSecret: this.dataCommande.codeSecret, cmdCode: this.dataCommande.cmdCode, vendeurToken: tok_vendeur, vendeur_choisi: 'en_attente_acceptation'})
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
=======
      // verification de la precision du vendeur_acheteur
      this.firestore.firestore.collection('vendeur').where('email', '==', this.email).get().then(
        (queySnapchot) => {
          var vendeur_acheteur_precision;
        queySnapchot.forEach((doc)=> {
          vendeur_acheteur_precision = doc.data().precision;
        })
        // si le vendeur_acheteur est un depot_marchand
        if(vendeur_acheteur_precision === 'depot_marchand') {
          this.dataCommande.codeSecret = this.getRandomIntInclusive(100000, 900000);
          this.dataCommande.cmdCode = this.uuidv4();
          console.log(this.dataCommande.cmdCode);
          this.cart.forEach(element => {
            this.dataCommande.produit = element.name;
            this.dataCommande.qty = element.qty;
            this.dataCommande.amount = element.amount;
            this.dataCommande.vendeur_choisi = localStorage.getItem('depotFinal');
            this.dataCommande.precision_vendeur = localStorage.getItem('precision_vendeur')
            this.dataCommande.email_acheteur = localStorage.getItem('email');
            console.log(this.dataCommande.vendeur_choisi);
            this.firestore.collection('commandes').add(this.dataCommande);
          });
    
           //recuperation du token du vendeur choisi et envoi de notification
           this.firestore.firestore.collection('testtoken').where('vendeur', '==', this.dataCommande.vendeur_choisi).onSnapshot(
            (querySnapchot) => {
              let tok_vendeur;
              querySnapchot.forEach((doc) => {
                tok_vendeur = doc.data().usertoken;
              })
              this.firestore.collection('infoCommande').add({email_acheteur: this.dataCommande.email_acheteur,precision_vendeur:this.dataCommande.precision_vendeur, frais_total:this.dataCommande.frais_total, argent_vendeur:this.dataCommande.prix_total, argent_livreur:this.dataCommande.frais_livraison, date_enregistrement:this.dataCommande.date_enregistrement, heure_enregistrement:this.dataCommande.heure_enregistrement, enregistrer: this.dataCommande.enregistrer, en_attente: this.dataCommande.en_attente, en_livraison: this.dataCommande.en_livraison, remise: this.dataCommande.remise, livrer: this.dataCommande.livrer, livraison_detail1: this.dataCommande.livraison_detail1, livraison_detail2: this.dataCommande.livraison_detail2, codeSecret: this.dataCommande.codeSecret, cmdCode: this.dataCommande.cmdCode, vendeurToken: tok_vendeur, vendeur_choisi: 'en_attente_acceptation'})
            }
          )
          this.presentAlert();
          this.router.navigate(['vendeur-tabs/accueil']);
>>>>>>> 3b82dec (qr code)
        }
        // si le vendeur_acheteur est un distributeur_agreer
        if(vendeur_acheteur_precision === 'distributeur_agreer') {
          this.dataCommande.codeSecret = this.getRandomIntInclusive(100000, 900000);
          this.dataCommande.cmdCode = this.uuidv4();
          console.log(this.dataCommande.cmdCode);
          this.cart.forEach(element => {
            this.dataCommande.produit = element.name;
            this.dataCommande.qty = element.qty;
            this.dataCommande.amount = element.amount;
            this.dataCommande.vendeur_choisi = localStorage.getItem('depotFinal');
            this.dataCommande.precision_vendeur = localStorage.getItem('precision_vendeur')
            this.dataCommande.email_acheteur = localStorage.getItem('email');
            console.log(this.dataCommande.vendeur_choisi);
            this.firestore.collection('commandes_sobebra').add(this.dataCommande);
          });
        }

      })
      
      
    }

<<<<<<< HEAD
=======
    }else {
      if(this.statut === 'client') {
        alert('solde inssufisant ou frais deja engager pour une autre commander! veuillez recharger votre portefeuille');
        this.router.navigate(['portfeuil-client']);
      }
      if(this.statut === 'vendeur') {
        alert('solde inssufisant ou frais deja engager pour une autre commander! veuillez recharger votre portefeuille');
        this.router.navigate(['portfeuil-vendeur']);
      }
      if(this.statut === 'livreur') {
        alert('solde inssufisant ou frais deja engager pour une autre commander! veuillez recharger votre portefeuille');
        this.router.navigate(['portfeuil-livreur']);
      }
    }
    

>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Commande effectuée',
      message: 'Votre commande a été enregistré.',
      buttons: ['OK']
    });

    await alert.present();


  }

  uuidv4() {
    // tslint:disable-next-line:only-arrow-functions
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      // tslint:disable-next-line:one-variable-per-declaration
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  // On renvoie un entier aléatoire entre une valeur min (incluse)
// et une valeur max (incluse).
// Attention : si on utilisait Math.round(), on aurait une distribution
// non uniforme !
 getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}






}
