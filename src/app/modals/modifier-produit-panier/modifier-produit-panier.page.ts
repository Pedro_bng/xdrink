import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-modifier-produit-panier',
  templateUrl: './modifier-produit-panier.page.html',
  styleUrls: ['./modifier-produit-panier.page.scss'],
})
export class ModifierProduitPanierPage implements OnInit {

  stock: any;
  // tslint:disable-next-line:variable-name
  modifier_stock: any;

  inputQty = false;

  dataStock1 = {
    prix_unitaire: ''
  };

  dataStock2 = {
    qty: 0
  };

  dataStock3 = {
    unite: ''
  };

  dataStock =  {
    qty: '',
    prix_unitaire: '',
    unite: '',
  };
  option: any;
  visual: any;

  nom_produit = 'Fanta citron';
  unite = 'casier';
  prix_unitaire = 1000;
  qty = 5;
  // tslint:disable-next-line:variable-name
  nom_prod: string;
  qtyAct = {
    qty: 0
  };
  constructor(private modalCtrl: ModalController,
              private firestore: AngularFirestore) {
                this.option = localStorage.getItem('option');
                if (this.option === 'achat') {
                  this.visual = 'Quantité acheté';
                }
                if (this.option === 'vente') {
                  this.visual = 'Quantité vendu';
                }
                this.stock = localStorage.getItem('stock_vendeur');
                this.nom_prod = localStorage.getItem('nom_prod');
                this.option = localStorage.getItem('option');
                this.firestore.collection(this.stock).doc(this.nom_prod).snapshotChanges()
                .subscribe( action => {
                  // tslint:disable-next-line:no-string-literal
                  console.log(action.payload.data()['qty']);
                  // tslint:disable-next-line:no-string-literal
                  this.qtyAct.qty = action.payload.data()['qty'];
                  console.log(this.qtyAct);
                });
               }

  ngOnInit() {
    this.modifier_stock = JSON.parse(localStorage.getItem('stockref'));
    console.log(this.modifier_stock);
    console.log(this.nom_prod);
  }


  // tslint:disable-next-line:variable-name
  valider(_moyen_de_paiement){
    console.log('valider libraison');
    console.log(this.dataStock);
    if (this.dataStock1.prix_unitaire !== '') {
      this.firestore.collection(this.stock).doc(this.nom_prod).update(this.dataStock1);
    }
    if (this.dataStock2.qty !== undefined) {
      this.option = localStorage.getItem('option');
      if (this.option === 'achat') {
        this.qtyAct.qty = this.qtyAct.qty + this.dataStock2.qty;
        console.log(this.qtyAct);
        this.firestore.collection(this.stock).doc(this.nom_prod).update(this.qtyAct);
      }

      if (this.option === 'vente') {
        this.qtyAct.qty = this.qtyAct.qty - this.dataStock2.qty;
        console.log(this.qtyAct);
        this.firestore.collection(this.stock).doc(this.nom_prod).update(this.qtyAct);
      }
    }
    if (this.dataStock3.unite !== '') {
      this.firestore.collection(this.stock).doc(this.nom_prod).update(this.dataStock3);
    }
    this.dissmiss();
  }
  dissmiss(){
    this.modalCtrl.dismiss({
      data: {
        nom_produit: this.nom_produit,
        unite: this.unite,
        prix_unitaire: this.prix_unitaire,
        qty: this.qty
      }
    });
    console.log(this.dataStock);
  }

  increase(item){
    if (item.qty){
      item.qty = item.qty + 1;
    }
    else{
      item.qty = 2;
    }
  }
  decrease(item){
    if (!(item.qty - 1 <= 0)){
      item.qty = item.qty - 1;
    }

  }

  inputChange(event, item){
    console.log('event :>> ', event);
    // tslint:disable-next-line:radix
    item.qty = parseInt(event);
  }

}
