import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Geolocation} from '@capacitor/core';

@Component({
  selector: 'app-livraison-choix',
  templateUrl: './livraison-choix.page.html',
  styleUrls: ['./livraison-choix.page.scss'],
})
export class LivraisonChoixPage implements OnInit {

  latitude: number;
  longitude: number;

  livraison = {
    localisation: '',
    detail: ''
  };

  constructor(private modalCtrl: ModalController) {
   }

  ngOnInit() {
  }

  valider(){
    console.log('valider libraison');
    this.dismiss();
    localStorage.livraison_localisation = this.livraison.localisation;
    localStorage.livraison_detail = this.livraison.detail;
    console.log(this.livraison.localisation, this.livraison.detail);
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'livraison': this.livraison
    });
  }

   async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    this.latitude = position.coords.latitude;
    this.longitude = position.coords.longitude;
    console.log(this.latitude, this.longitude);

  }

}
