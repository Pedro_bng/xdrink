import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-historiques-client',
  templateUrl: './historiques-client.page.html',
  styleUrls: ['./historiques-client.page.scss'],
})
export class HistoriquesClientPage implements OnInit {
  email: string;
  historique_achat: any[];

  constructor(private firestore: AngularFirestore) { 
    this.email = localStorage.getItem('email');
  }

  ngOnInit() {
    this.historique();
  }

  historique() {
    this.firestore.firestore.collection('infoCommande').where('email_acheteur', '==', this.email ).onSnapshot(
      (querySnapchot) => {
<<<<<<< HEAD
        this.historique_achat = [];
        querySnapchot.forEach((doc) => {
          this.historique_achat.push({
            cmdCode: doc.data().cmdCode,
            statut: doc.data().statut
=======
        let enregistrer;
        let en_attente;
        let en_livraison;
        let remise;
        let livrer;
        let statut;
        this.historique_achat = [];
        querySnapchot.forEach((doc) => {
            enregistrer = doc.data().enregistrer,
            en_attente = doc.data().en_attente,
            en_livraison = doc.data().en_livraison,
            remise = doc.data().remise,
            livrer = doc.data().livrer
            if(enregistrer === 'ok' && en_attente === '' && en_livraison === '' && remise === '' && livrer === ''){
              statut = 'enregistrer'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === '' && remise === '' && livrer === ''){
              statut = 'en_attente'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === '' && livrer === ''){
              statut = 'en_livraison'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === 'ok' && livrer === ''){
              statut = 'remise'
            }
            if(enregistrer === 'ok' && en_attente === 'ok' && en_livraison === 'ok' && remise === 'ok' && livrer === 'ok'){
              statut = 'livrer'
            }
          this.historique_achat.push({
            cmdCode: doc.data().cmdCode,
            statut: statut,
            heure: doc.data().heure_enregistrement,
            date: doc.data().date_enregistrement
>>>>>>> febfe0e9ac902214682b4ac55cd73a18588974dd
          });
        })
      }
    )
  }

  voirDetail(item) {
    console.log(item.cmdCode);
    localStorage.cmdCode = item.cmdCode;
  }

}
