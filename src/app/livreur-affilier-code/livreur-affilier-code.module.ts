import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LivreurAffilierCodePageRoutingModule } from './livreur-affilier-code-routing.module';

import { LivreurAffilierCodePage } from './livreur-affilier-code.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LivreurAffilierCodePageRoutingModule
  ],
  declarations: [LivreurAffilierCodePage]
})
export class LivreurAffilierCodePageModule {}
