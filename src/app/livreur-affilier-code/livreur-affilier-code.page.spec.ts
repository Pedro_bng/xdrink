import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LivreurAffilierCodePage } from './livreur-affilier-code.page';

describe('LivreurAffilierCodePage', () => {
  let component: LivreurAffilierCodePage;
  let fixture: ComponentFixture<LivreurAffilierCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivreurAffilierCodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LivreurAffilierCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
