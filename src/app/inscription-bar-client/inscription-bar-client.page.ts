import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Geolocation} from '@capacitor/core';
import { AuthentificationService } from '../authentification/authentification.service';

import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from '@capacitor/core';

const { PushNotifications, Modals } = Plugins;


@Component({
  selector: 'app-inscription-bar-client',
  templateUrl: './inscription-bar-client.page.html',
  styleUrls: ['./inscription-bar-client.page.scss'],
})
export class InscriptionBarClientPage implements OnInit {
  dataBar = {
    nom_Bar: '',
    nom: '',
    telephone_gerant: '',
    email: '',
    ifu: '',
    adresse_livraison: '',
    password: '',
    password2: '',
    type: 'client',
    latitude: 0,
    longitude: 0,
    usertoken: '',
    precision: 'bar-buvette'
  };
  errorMessage: any;

  constructor(
    private firestore: AngularFirestore,
    private authService: AuthentificationService,
    private router: Router,
    public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  soumettre() {
    if (this.dataBar.password === this.dataBar.password2){
      if(this.dataBar.longitude !== 0 && this.dataBar.latitude !== 0) {
      const email = this.dataBar.email;
      const password = this.dataBar.password;
      this.authService.signUpUser(email, password).then(
        () => {
          localStorage.pseudo = this.dataBar.nom;
          localStorage.email = this.dataBar.email;
          localStorage.type = 'client';
          this.generateTokenFCM();
          this.firestore.collection('client').add(this.dataBar);
          this.firestore.collection('user').add(this.dataBar);
          this.firestore.collection('portefeuille').add({
            proprietaire: this.dataBar.email,
            solde: 0,
            retrait: 0,
            ajout: 0
          });
          localStorage.latitude = this.dataBar.latitude;
          localStorage.longitude = this.dataBar.longitude;
          localStorage.nom_bar = this.dataBar.nom_Bar;
          this.router.navigate(['tabs']);
        },
        (error) => {
         this.errorMessage = error;
        }
      );
      } else { this.presentAlertConfirm2 ();}
    }
  }

  async presentAlertConfirm2() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Activez votre localisation et ressayez</strong>!!!',
      buttons: [
         {
          text: 'ok',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }


  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'IMPORTANT!',
      message: ' <strong>Avant de confirmer la géolocalisation vous devez être dans votre Bar/Buvette</strong>!!!',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmer position',
          handler: () => {
            console.log('Confirm Okay');
            this.getPosition();
          }
        }
      ]
    });

    await alert.present();
  }

  async getPosition() {
    const position = await Geolocation.getCurrentPosition();
    this.dataBar.latitude = position.coords.latitude;
    this.dataBar.longitude = position.coords.longitude;
    console.log(this.dataBar.latitude, this.dataBar.longitude);
  }

generateTokenFCM() {
  // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    PushNotifications.requestPermission().then(result => {
      if (result.granted) {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    PushNotifications.addListener(
      'registration',
      (token: PushNotificationToken) => {
        alert('Push registration success, token: ' + token.value);
        this.firestore.collection('testtoken').add({
          userid: this.dataBar.email,
          usertoken:token.value
        })
      },
    );

    PushNotifications.addListener('registrationError', (error: any) => {
      alert('Error on registration: ' + JSON.stringify(error));
    });

    PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotification) => {
        // alert('Push received: ' + JSON.stringify(notification));

        // tslint:disable-next-line:prefer-const
        let alert =  Modals.alert ( {
          title: notification.title,
          message: notification.body
        });
      },
    );

    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: PushNotificationActionPerformed) => {
        alert('Push action performed: ' + JSON.stringify(notification));
      },
    );
  }
}



