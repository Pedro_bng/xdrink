import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QrcodeVuePage } from './qrcode-vue.page';

describe('QrcodeVuePage', () => {
  let component: QrcodeVuePage;
  let fixture: ComponentFixture<QrcodeVuePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrcodeVuePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QrcodeVuePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
