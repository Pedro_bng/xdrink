import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QrcodeVuePage } from './qrcode-vue.page';

const routes: Routes = [
  {
    path: '',
    component: QrcodeVuePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QrcodeVuePageRoutingModule {}
