import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QrcodeVuePageRoutingModule } from './qrcode-vue-routing.module';

import { QrcodeVuePage } from './qrcode-vue.page';
import { QRCodeModule } from 'angularx-qrcode';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    QRCodeModule,
    IonicModule,
    QrcodeVuePageRoutingModule
  ],
  declarations: [QrcodeVuePage]
})
export class QrcodeVuePageModule {}
