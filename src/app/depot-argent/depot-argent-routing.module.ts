import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DepotArgentPage } from './depot-argent.page';

const routes: Routes = [
  {
    path: '',
    component: DepotArgentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DepotArgentPageRoutingModule {}
