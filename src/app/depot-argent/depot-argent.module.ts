import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DepotArgentPageRoutingModule } from './depot-argent-routing.module';

import { DepotArgentPage } from './depot-argent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DepotArgentPageRoutingModule
  ],
  declarations: [DepotArgentPage]
})
export class DepotArgentPageModule {}
