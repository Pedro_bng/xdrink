import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DepotArgentPage } from './depot-argent.page';

describe('DepotArgentPage', () => {
  let component: DepotArgentPage;
  let fixture: ComponentFixture<DepotArgentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepotArgentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DepotArgentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
